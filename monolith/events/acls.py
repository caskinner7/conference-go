import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    payload = {
        'query': f'{city} {state}',
        'per_page': 1,
    }
    response = requests.get(url, params=payload, headers=headers)

    photo = json.loads(response.content)
    return photo['photos'][0]['url']




def get_weather_data(city, state):
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct"
    payload = {
        'q': f'{city}, {state}, {840}',
        'appid': OPEN_WEATHER_API_KEY
    }
    response = requests.get(url, params=payload, headers=headers)
    geocode = json.loads(response.content)

    lat = geocode[0]['lat']
    lon = geocode[0]['lon']

    url = "https://api.openweathermap.org/data/2.5/weather"
    payload = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, payload, headers=headers)
    weather = json.loads(response.content)
    try:
        return {
            "temp": weather["main"]["temp"],
            "description": weather["weather"][0]["description"]
        }
    except (KeyError, IndexError):
        return None
